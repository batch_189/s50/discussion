import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
// import AppNavbar from './components/AppNavbar';
import 'bootstrap/dist/css/bootstrap.min.css'; // import bootstrap css

const root = ReactDOM.createRoot(document.getElementById('root')); // html div root-id

root.render( 
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// const name = "Tristan Parcon";
// const student = {
//   firstName: 'Nehemiah',
//   lastName: 'Ellorico',
// };
// function userName(user){
//   return user.firstName + ' ' + user.lastName
// };

// const element = <h1>Hello, {userName(student)}</h1>

// root.render(element);