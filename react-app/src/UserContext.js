import React from "react";

// Create a context object
// A context object as the name state in a data type of an object that can be used to store info  that can be shared to other components within the app
// context objet is a diff approach to passing info between components an d allows easier access by avoiding the use of prop-drilling.
const UserContext = React.createContext();

// The "Provider" component allows other components to consume/use the context object and supply the necessary info needed to the context object.
export const UserProvider = UserContext.Provider;

export default UserContext;