import { useState, useEffect } from "react";
import { Card, Button } from "react-bootstrap";
import { Link } from 'react-router-dom';

export default function CourseCard({courseProp}) {
  
  // console.log(courseProp)
  // console.log(typeof props)
  // console.log(props.courseProp.name);
  /*
    Use the state hook for this component to be able to store its state.
    states are use to keep track of information related to individual components.

    Syntax:
      const  [getter, setter] = useState(initialGetterValue);
  */
 
//  const [count, setCount] = useState(0);
//  // Use state hook for getting and setting the seats for this course
//  const [seats, setSeats] = useState(30);

//  function enroll(){
//   // if(seats === 0){
//   //   alert ("No more seats available!")
//   //   return;
//   // };
//   setCount(count + 1);
//   setSeats(seats - 1);
//  };

//  useEffect(() => {
//   if (seats === 0){
//     alert('No more seats available')
//   };
//  }, [seats]);
 
  // Deconstruct the course properties into their own variable.
  const { name, description, price, _id } = courseProp; 

  return (
    <Card>
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>{price}</Card.Text>
        {/* <Card.Text>Enrollees: {count}</Card.Text>
        <Card.Text>Seats: {seats}</Card.Text> */}
        <Button variant="primary" as={Link} to={`/courses/${_id}`}>Details</Button>
        {/* <Link className="btn btn-primary" to="/courseView">Details</Link> */}
      </Card.Body>
    </Card>
  );
}
