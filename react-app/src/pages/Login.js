import { useState, useEffect, useContext } from "react";
import { Form, Button } from "react-bootstrap";
import { Navigate } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

export default function Login() {

  // Allows us to consume the User context object and its properties to use for user validation.
  const { user, setUser } = useContext(UserContext);

  // State hooks to store the values of the input fields.
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const [isActive, setIsActive] = useState(false);

  function Authenticate(e){
    e.preventDefault();

    /*
     Syntax: 
      fetch('url', {options})
      .then(response => response.json())
      .then(data => {})
     */
    fetch ('http://localhost:4000/users/login', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
    .then(response => response.json())
    .then(data => {
      
      console.log(data) // result: access token

      // if(data !== false){
      if(typeof data.access !== "undefined"){

        localStorage.setItem("token", data.access)
        retrieveUserDetails(data.access)

        Swal.fire({
          title: "Login Successful",
          icon: "success",
          text: "Welcome to Zuitt!"
        })

      } else {
        Swal.fire({
          title: "Authentication Failed",
          icon: "question",
          text: "Check your login details and try again!"
        })
      }
    })


    // Set the email of the authenticated user in the local storage.
    // Syntax: localStorage.setItem("propertyName, value");
    // localStorage.setItem("email", email); 

    // Set the global user state to have properties obtained from local storage.
    // setUser({
    //   email: localStorage.getItem("email")
    // });

    // Clear input fields after submission
    setEmail("");
    setPassword("");

    //alert (`${email} Has been verified, you are now login!`)
  };

  const retrieveUserDetails = (token) => {

    fetch ('http://localhost:4000/users/details', {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    .then(response => response.json())
    .then(data => {
      console.log(data) // result: user details

      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      })
    })
  };

  useEffect(()  => {

    if(
        email !== "" &&
        password !== "" 
    ) {
        setIsActive(true)
    } else {
        setIsActive(false)
    }
  }, [email, password]);

  return (

    (user.id !== null) ?
      <Navigate to="/courses" /> 

      :

    <Form className="mt-3" onSubmit={(e) => Authenticate(e)}>
      <h1 className="text-center">Login</h1>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter email"
          value={email}
          onChange={(e) => {
            setEmail(e.target.value)
          }}
          required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="password1">
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Password"
          value={password}
          onChange={(e) => {
            setPassword(e.target.value)
          }}
          required
        />
      </Form.Group>

      {isActive ? (
        <Button variant="success" type="submit" id="submitBtn">
          Submit
        </Button>
      ) : (
        <Button variant="danger" type="submit" id="submitBtn" disabled>
          Submit
        </Button>
      )}
    </Form>
  );
};
