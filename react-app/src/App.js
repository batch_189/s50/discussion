// import { Fragment } from 'react'; // alternative <> </>
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
import { useState, useEffect } from "react";
import { Container } from "react-bootstrap";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { UserProvider } from "./UserContext";
import AppNavbar from "./components/AppNavbar";
import Courses from "./pages/Courses";
import Home from "./pages/Home";
import Register from './pages/Register';
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import ErrorPage from "./components/PageNotFound";
import CourseView from "./pages/CourseView";
import "./App.css";


function App() {

  // state hook for the user state that's defined here for a global scope.
  const [user, setUser] = useState({
    // email: localStorage.getItem('email'),
    id: null,
    isAdmin: null
  });

  // Function for clearing localStorage on logout.
  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {

    fetch('http://localhost:4000/users/details',  {
      headers:  {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(response  => response.json())
    .then(data => {

      if(typeof  data._id !== "undefined"){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
        
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, []);

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
       <AppNavbar />
       <Container>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/register" element={<Register />} />
          <Route path="/login" element={<Login />} />
          <Route path="/logout" element={<Logout />} />
          <Route path="/courses" element={<Courses />} />
          <Route path="/courses/:courseId" element={<CourseView />} />
          <Route path="*" element={<ErrorPage />} />
       </Routes>
       </Container>
      </Router>
    </UserProvider>
  );
};

export default App;
