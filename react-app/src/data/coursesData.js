const coursesData = [
  {
    id: "wdc001",
    name: "PHP-Laravel",
    description: "Lorem ipsum dolor sit amet consectetur adipisicing elit.",
    price: 4500,
    onOffer: true,
  },
  {
    id: "wdc002",
    name: "Python-Django",
    description: "Lorem ipsum dolor sit amet consectetur adipisicing elit.",
    price: 50000,
    onOffer: true,
  },
  {
    id: "wdc003",
    name: "Java-Springboot",
    description: "Lorem ipsum dolor sit amet consectetur adipisicing elit.",
    price: 55000,
    onOffer: true,
  },
];

export default coursesData;
